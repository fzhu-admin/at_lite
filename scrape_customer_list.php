<?php
namespace at_lite;

// get function declarations
#require_once 'curl_functions.php';

// define user agent
$agent  = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) ';
$agent .= 'Gecko/20100101 Firefox/32.0';

// define cookie file
$cookie = 'at_cookie.txt';

// get cookie if cookie file not found
if (file_exists($cookie) == false) {
    at_lite\getATCookie($agent, $cookie);
}

// get AdminTool page HTML
$page_html = getATPage(
      $agent
    , $cookie
    , 'https://admintool.peakhosting.com/index.php?r=customer/index'
);

// parse AdminTool HTML for customer info
$line_arr = explode("\n", $page_html);
foreach ($line_arr as $line) {
    #
    # do if line starts with <tr
    if (preg_match("/^\<tr /", $line)) {
        #
        # drop everything after first </a> tag
        $trimmed_line = explode('</a>', $line)[0];
        #
        # capture $customer_id and $customer_name
        preg_match("/id=(\d+)\">(.*)/", $trimmed_line, $capture_arr);
        $customer_id   = $capture_arr[1];
        $customer_name = $capture_arr[2];
        echo "$customer_id\t$customer_name\n";
    } # else do nothing
}

/*
$ts = new DBConnection;
$data = array(
    'at_num'=>'999',
        'customer_name'=>'Foo'
        );
$statement = "INSERT INTO customer (at_num, customer_name) VALUES (999, 'Foo Inc.')";
$ts->insertEntry('customer', $data, $statement);
*/
