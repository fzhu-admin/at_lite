<?php
namespace at_lite;

class DBConnection {

    // set mysql host, socket, user, and database properties
    private $socket   = '/tmp/mysql.sock';
    private $hostname = 'localhost';
    private $username = 'at';
    private $database = 'at_lite';
    private $db_handle;

    // start connection with database
    function __construct() {
        #
        # define data source name
        $dsn  = 'mysql:';
        $dsn .= "unix_socket=$this->socket;";
        $dsn .= "host=$this->hostname;";
        $dsn .= "dbname=$this->database";
        #
        # instantiate PDO instance
        $this->db_handle = new PDO($dsn, $this->username);
    }

    // define procedures for inserting into tables
    function insertEntry($table, $data, $statement)
    {
        #
        # inherit $db_handle from class definition
        $db_handle = $this->db_handle;
        #
        # prepare and execute insert statement
        $statement_handle = $db_handle->prepare($statement);
        try {
            $statement_handle->execute( $data );
        } catch( PDOException $e ) {
            echo $e->getMessage();
        }
    }
}

