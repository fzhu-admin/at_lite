DROP DATABASE at_lite;
CREATE DATABASE at_lite;
USE at_lite;

/*--------------------------------------------------------------------------------------------------*/
DROP TABLE IF EXISTS colo;
CREATE TABLE colo (
	-- Dependencies: none
/*--------------------------------------------------------------------------------------------------*/
	  colo_id						TINYINT	UNSIGNED		NOT NULL AUTO_INCREMENT
	, at_num						TINYINT UNSIGNED		NOT NULL
	, colo_name						VARCHAR(4)				NOT NULL
	, PRIMARY KEY (colo_id)
) ENGINE=InnoDB
;

INSERT INTO colo (at_num, colo_name) VALUES (1, "MLP1");
INSERT INTO colo (at_num, colo_name) VALUES (2, "IAD1");
INSERT INTO colo (at_num, colo_name) VALUES (3, "IAD2");
INSERT INTO colo (at_num, colo_name) VALUES (5, "AMS1");
INSERT INTO colo (at_num, colo_name) VALUES (4, "DAL1");
INSERT INTO colo (at_num, colo_name) VALUES (6, "DAL2");
INSERT INTO colo (at_num, colo_name) VALUES (8, "SJC2");

/*--------------------------------------------------------------------------------------------------*/
DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
	-- Dependencies: none
/*--------------------------------------------------------------------------------------------------*/
	  customer_id					SMALLINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, at_num						SMALLINT UNSIGNED		NOT NULL
	, customer_name					VARCHAR(45)				NOT NULL
	, PRIMARY KEY (customer_id)
) ENGINE=InnoDB
;

INSERT INTO customer (at_num, customer_name) VALUES (20, "Social Gaming Network");
INSERT INTO customer (at_num, customer_name) VALUES (38, "Jibe Mobile");
INSERT INTO customer (at_num, customer_name) VALUES (79, "Gravity");
INSERT INTO customer (at_num, customer_name) VALUES (90, "Uber Technologies");
INSERT INTO customer (at_num, customer_name) VALUES (176, "Machine Zone");
INSERT INTO customer (at_num, customer_name) VALUES (177, "Scorebig");

/*--------------------------------------------------------------------------------------------------*/
DROP TABLE IF EXISTS manufacturer;
CREATE TABLE manufacturer (
	-- Dependencies: none
/*--------------------------------------------------------------------------------------------------*/
	  manufacturer_id				SMALLINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, manufacterer_name				VARCHAR(45)				NOT NULL
	, PRIMARY KEY (manufacturer_id)
) ENGINE=InnoDB
;

/*--------------------------------------------------------------------------------------------------*/
DROP TABLE IF EXISTS struct;
CREATE TABLE struct (
	-- Dependencies: none
/*--------------------------------------------------------------------------------------------------*/
	  struct_id						SMALLINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, struct_name					VARCHAR(45)				NOT NULL
	, association					VARCHAR(45)				NOT NULL
	, PRIMARY KEY (struct_id)
) ENGINE=InnoDB
;

INSERT INTO struct (struct_name, association) VALUES ("1Gb", "net_speed");
INSERT INTO struct (struct_name, association) VALUES ("10Gb", "net_speed");

/*--------------------------------------------------------------------------------------------------*/
DROP TABLE IF EXISTS software;
CREATE TABLE software (
	-- Dependencies: none
/*--------------------------------------------------------------------------------------------------*/
	  software_id					SMALLINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, software_name					VARCHAR(45)				NOT NULL
	, version						VARCHAR(9)				NOT NULL
	, PRIMARY KEY (software_id)
) ENGINE=InnoDB
;

/*--------------------------------------------------------------------------------------------------*/
DROP TABLE IF EXISTS vlan;
CREATE TABLE vlan (
	-- Dependencies: none
/*--------------------------------------------------------------------------------------------------*/
	  vlan_id						SMALLINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, vlan_num						SMALLINT				NOT NULL
	, vlan_desc						VARCHAR(45)				NOT NULL
	, PRIMARY KEY (vlan_id)
) ENGINE=InnoDB
;

/*****************************************************************************************************
* Tables in this section are dependent on previously defined tables.
*****************************************************************************************************/

/* Dependent on manufacturer and master_type */
/****************************************************************************************************/
DROP TABLE IF EXISTS model;
/****************************************************************************************************/
CREATE TABLE model (
	  model_id							SMALLINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, model_name						VARCHAR(45)				NOT NULL
	, manufacturer_id					SMALLINT UNSIGNED		NOT NULL
	, PRIMARY KEY (model_id)
	, FOREIGN KEY (manufacturer_id)		REFERENCES manufacturer	(manufacturer_id)
) ENGINE=InnoDB
;

/* Dependent on colo, customer, model */
/****************************************************************************************************/
DROP TABLE IF EXISTS device;
/****************************************************************************************************/
CREATE TABLE device (
	  device_id						MEDIUMINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, import_timestamp				TIMESTAMP				NOT NULL DEFAULT CURRENT_TIMESTAMP
	, device_type_id				SMALLINT UNSIGNED		NOT NULL
	, model_id						SMALLINT UNSIGNED		NOT NULL
	, colo_id						TINYINT UNSIGNED		NOT NULL
	, cage_name						VARCHAR(2)				NOT NULL
	, isle_num						TINYINT UNSIGNED		NOT NULL
	, rack_num						TINYINT	UNSIGNED		NOT NULL
	, rack_elevation				TINYINT	UNSIGNED		NOT NULL
	, customer_id					SMALLINT UNSIGNED		NOT NULL
	, at_num						MEDIUMINT UNSIGNED		NOT NULL
	, serial_num					VARCHAR(15)				NOT NULL
	, peak_name						VARCHAR(8)				NOT NULL
	, fqdn							VARCHAR(45)				NOT NULL
	, oob_ipaddr					VARCHAR(15)				NOT NULL
	, oob_hwaddr					VARCHAR(17)				NOT NULL
	, PRIMARY KEY (device_id)
	, FOREIGN KEY (device_type_id)	REFERENCES struct		(struct_id)
	, FOREIGN KEY (model_id)		REFERENCES model		(model_id)
	, FOREIGN KEY (colo_id)			REFERENCES colo			(colo_id)
	, FOREIGN KEY (customer_id)		REFERENCES customer		(customer_id)
) ENGINE=InnoDB
;

INSERT INTO struct (struct_name, association) VALUES ("Server", "device");
INSERT INTO struct (struct_name, association) VALUES ("Switch", "device");
INSERT INTO struct (struct_name, association) VALUES ("Router", "device");
INSERT INTO struct (struct_name, association) VALUES ("Firewall", "device");
INSERT INTO struct (struct_name, association) VALUES ("Load Balancer", "device");

/* Dependent on device */
/****************************************************************************************************/
DROP TABLE IF EXISTS switchport;
/****************************************************************************************************/
CREATE TABLE switchport (
	  switchport_id					MEDIUMINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, import_timestamp				TIMESTAMP				NOT NULL DEFAULT CURRENT_TIMESTAMP
	, device_id						MEDIUMINT UNSIGNED		NOT NULL
	, switchport_name				VARCHAR(25)				NOT NULL
	, switchport_desc				VARCHAR(45)				NOT NULL
	, vpc							SMALLINT UNSIGNED		NOT NULL
	, ipaddr						VARCHAR(15)				NOT NULL
	, mac_addr						VARCHAR(17)				NOT NULL
	, PRIMARY KEY (switchport_id)
	, FOREIGN KEY (device_id)		REFERENCES device		(device_id)
) ENGINE=InnoDB
;

/****************************************************************************************************/
DROP TABLE IF EXISTS hard_drive;
/****************************************************************************************************/
/* Dependent on device and model */
CREATE TABLE hard_drive (
	  hard_drive_id						MEDIUMINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, import_timestamp					TIMESTAMP				NOT NULL DEFAULT CURRENT_TIMESTAMP
	, device_id							MEDIUMINT UNSIGNED		NOT NULL
	, model_id							SMALLINT UNSIGNED		NOT NULL
	, drive_size						SMALLINT UNSIGNED		NOT NULL
	, interface_type_id					SMALLINT UNSIGNED		NOT NULL
	, serial_num						VARCHAR(15)				NOT NULL
	, PRIMARY KEY (hard_drive_id) 
	, FOREIGN KEY (device_id)			REFERENCES device		(device_id)
	, FOREIGN KEY (model_id)			REFERENCES model		(model_id)
	, FOREIGN KEY (interface_type_id)	REFERENCES struct		(struct_id)
) ENGINE=InnoDB
;

INSERT INTO struct (struct_name, association) VALUES ("NLSAS", "hard_drive");
INSERT INTO struct (struct_name, association) VALUES ("SATA", "hard_drive");
INSERT INTO struct (struct_name, association) VALUES ("SAS", "hard_drive");

/* Dependent on device and model */
/****************************************************************************************************/
DROP TABLE IF EXISTS nic;
/****************************************************************************************************/
CREATE TABLE nic (
	  nic_id						MEDIUMINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, import_timestamp				TIMESTAMP				NOT NULL DEFAULT CURRENT_TIMESTAMP
	, device_id						MEDIUMINT UNSIGNED		NOT NULL
	, model_id						SMALLINT UNSIGNED		NOT NULL
	, speed_id						SMALLINT UNSIGNED		NOT NULL
	, mac_addr						VARCHAR(17)				NOT NULL
	, serial_num					VARCHAR(15)				NOT NULL
	, PRIMARY KEY (nic_id)
	, FOREIGN KEY (device_id)		REFERENCES device		(device_id)
	, FOREIGN KEY (model_id)		REFERENCES model		(model_id)
	, FOREIGN KEY (speed_id)		REFERENCES struct		(struct_id)
) ENGINE=InnoDB
;

/* Dependent on device and nic */
/****************************************************************************************************/
DROP TABLE IF EXISTS interface;
/****************************************************************************************************/
CREATE TABLE interface (
	  interface_id					MEDIUMINT UNSIGNED		NOT NULL AUTO_INCREMENT
	, import_timestamp				TIMESTAMP				NOT NULL DEFAULT CURRENT_TIMESTAMP
	, device_id						MEDIUMINT UNSIGNED		NOT NULL
	, nic_id						MEDIUMINT UNSIGNED		NOT NULL
	, interface_name				VARCHAR(25)				NOT NULL
	, ip_addr						VARCHAR(15)				NOT NULL
	, mac_addr						VARCHAR(17)				NOT NULL
	, PRIMARY KEY (interface_id)
	, FOREIGN KEY (nic_id)			REFERENCES nic			(nic_id)
	, FOREIGN KEY (device_id)		REFERENCES device		(device_id)
) ENGINE=InnoDB
;

/*****************************************************************************************************
* Tables in this section define many-to-many relationships between previously defined tables.
*****************************************************************************************************/

/* Dependent on switchport and vlan */
/****************************************************************************************************/
DROP TABLE IF EXISTS vlan_switchport_lnk;
/****************************************************************************************************/
CREATE TABLE vlan_switchport_lnk (
	  vlan_id						SMALLINT UNSIGNED		NOT NULL
	, switchport_id					MEDIUMINT UNSIGNED		NOT NULL
	, PRIMARY KEY (vlan_id, switchport_id)
	, FOREIGN KEY (vlan_id)			REFERENCES vlan			(vlan_id)
	, FOREIGN KEY (switchport_id)	REFERENCES switchport	(switchport_id)
) ENGINE=InnoDB
;

/* Dependent on device and software */
/****************************************************************************************************/
DROP TABLE IF EXISTS device_software_lnk;
/****************************************************************************************************/
CREATE TABLE device_software_lnk (
	  device_id						MEDIUMINT UNSIGNED		NOT NULL
	, software_id					SMALLINT UNSIGNED		NOT NULL
	, PRIMARY KEY (device_id, software_id)
	, FOREIGN KEY (device_id)		REFERENCES device		(device_id)
	, FOREIGN KEY (software_id)		REFERENCES software		(software_id)
) ENGINE=InnoDB
;

